#!/bin/bash

VERSION=4.2.3b

cd .. 
if [ -d glx-$VERSION ] ; then
    rm -rf glx-$VERSION
fi
mkdir glx-$VERSION
cd glx-$VERSION
#wget http://dfn.dl.sourceforge.net/sourceforge/glx/GLE-$VERSION-src.zip 
wget http://prdownloads.sourceforge.net/glx/GLE-$VERSION-src.zip?download
cd ..
tar cvfz glx_$VERSION.orig.tar.gz glx-$VERSION/
rm -r glx-$VERSION
